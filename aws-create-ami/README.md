tool to create an AWS EC2 AMI image from a SOLIDserver vmdk file stored in a S3 bucket

* create a bucket with a name compatible with the IAM role vmimport
* store the vmdk file
* change the variable at the begining of the create_ami.py file
* create a virtualenv (```virtualenv env```) and activate it
* install requirements (```pip install -r requirements.txt```)
* AWS credentials should be set with appropriate rights in the IAM (see https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

the create_ami.py will import the S3 vmdk file in a snapshot, provide its id and create the AMI based on this snapshot

a SOLIDserver can be started directly from this AMI.
