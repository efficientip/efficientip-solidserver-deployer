# script to create an EfficientIP SOLIDserver image from S3 vmdk uploaded file

import boto3
from botocore.exceptions import ClientError
import time

# set values
aws_tags_client = 'client'
aws_tags_env = 'dev'
aws_tags_owner = 'me'
aws_tags_app = 'sds_ami_creator'
aws_tags_version = '7.1.2'
aws_image_id = 1    # imort is idempotent, if multiple iteration, change this id

bucket_name = "import-to-ec2-ach-dev"
vmdk_image_name = "SOLIDserver-Cloud-7.1.2.vmdk"
###

snapshot_id = None
ami_id = None

ec2_client = boto3.client('ec2')


def import_snapshot_from_s3():
    """import a SOLIDserver official image (vmdk) from the S3 bucket
       to a EC2 snapshot
       return the snapshot id"""
    r_import_job = ec2_client.import_snapshot(Description="Import SDS Snapshot - automated",
                                              DiskContainer={
                                                  'Description': "SDS712",
                                                  'Format': "VMDK",
                                                  'UserBucket': {
                                                      'S3Bucket': bucket_name,
                                                      'S3Key': vmdk_image_name
                                                  }
                                              },
                                              DryRun=False,
                                              RoleName='vmimport',
                                              ClientToken='{}{}-{}{}-{}'.format(aws_tags_client,
                                                                                aws_tags_env,
                                                                                aws_tags_owner,
                                                                                aws_tags_app,
                                                                                aws_image_id)
                                              )
    if not 'ImportTaskId' in r_import_job:
        print('error in import start job')
        exit()

    ImportTaskId = r_import_job['ImportTaskId']

    snapshot_id = None
    progress = None

    import_finished = False
    while (import_finished == False):
        r_import_job_progress = ec2_client.describe_import_snapshot_tasks(ImportTaskIds=[ImportTaskId],
                                                                          MaxResults=1,
                                                                          DryRun=False)

        import_task = r_import_job_progress['ImportSnapshotTasks'][0]
        snapshot_task_detail = import_task['SnapshotTaskDetail']

        if 'Progress' in snapshot_task_detail:
            if progress != snapshot_task_detail['Progress']:
                progress = snapshot_task_detail['Progress']
                print('import progress: ', progress, '%')

        if 'SnapshotId' in snapshot_task_detail:
            if snapshot_id is None:
                snapshot_id = snapshot_task_detail['SnapshotId']

        if 'Status' in snapshot_task_detail:
            if snapshot_task_detail['Status'] == 'completed':
                import_finished = True
                break

        time.sleep(15)

    # add information on the snapshot
    ec2_resource = boto3.resource('ec2')
    ec2_snapshot = ec2_resource.Snapshot(snapshot_id)
    try:
        ec2_snapshot.create_tags(Tags=[
            {'Key': 'Name', 'Value': 'SDS-{}'.format(aws_tags_version)},
            {'Key': 'Client', 'Value': aws_tags_client},
            {'Key': 'Owner', 'Value': aws_tags_owner},
            {'Key': 'Application', 'Value': aws_tags_app},
            {'Key': 'Environment', 'Value': aws_tags_env}]
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'InvalidSnapshot.NotFound':
            print(e.response['Error']['Message'])
            print("the snapshot has probably been removed (change the aws_image_id to recreate it)")
        else:
            print("Unexpected error: %s" % e)
        exit()

    return snapshot_id


def create_ami_from_snapshot(snapshot_id):
    try:
        ami_register = ec2_client.register_image(Architecture='x86_64',
                                                 BlockDeviceMappings=[{
                                                     'DeviceName': '/dev/sda1',
                                                     'Ebs': {
                                                         'DeleteOnTermination': True,
                                                         'VolumeType': 'gp2',
                                                         'SnapshotId': snapshot_id
                                                     }
                                                 }],
                                                 Description="EfficientIP SDS-{}".format(
                                                     aws_tags_version),
                                                 DryRun=False,
                                                 EnaSupport=False,
                                                 Name="EIP-SDS-{}".format(aws_tags_version),
                                                 RootDeviceName="/dev/sda1",
                                                 VirtualizationType='hvm')
    except ClientError as e:
        if e.response['Error']['Code'] == 'InvalidAMIName.Duplicate':
            print(e.response['Error']['Message'])
            exit()
        elif e.response['Error']['Code'] == 'InvalidSnapshot.NotFound':
            print(e.response['Error']['Message'])
            print("the snapshot has probably been removed (change the aws_image_id to recreate it)")
        else:
            print("Unexpected error: %s" % e)
        exit()

    if 'ImageId' not in ami_register:
        print("error in AMI creation")
        exit()

    ami_id = ami_register['ImageId']

    ec2_resource = boto3.resource('ec2')
    ami_image = ec2_resource.Image(ami_id)
    ami_image.create_tags(Tags=[
        {'Key': 'Name', 'Value': 'EfficientIP SOLIDserver {}'.format(aws_tags_version)},
        {'Key': 'Client', 'Value': aws_tags_client},
        {'Key': 'Owner', 'Value': aws_tags_owner},
        {'Key': 'Application', 'Value': 'SOLIDserver'},
        {'Key': 'Environment', 'Value': aws_tags_env}]
    )

    return ami_id


if snapshot_id is None:
    snapshot_id = import_snapshot_from_s3()
print('snapshot of disk created from s3 image: ', snapshot_id)

if ami_id is None:
    ami_id = create_ami_from_snapshot(snapshot_id)
print("ami created: id=", ami_id)
