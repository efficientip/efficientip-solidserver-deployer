# ansible installer of DNS/DHCP EIP packages on Linux host

## Supported Linux versions: 
* RedHat/CentOS 7
* RedHat/CentOS 6
* Debian 10 
* Debian 9

# Limitations:
 * You have to install  the same version across all servers 
 * ssh access needs to be homogeneous (same ssh key or password)

# Configuration
* clone or download this repo
* enter the directory package-install-linux/
* download the packages from the mirror and install these in shared_files/packages/
## edit the inventory host file located in inventory/hosts
* adjust for DNS and DHCP servers on which you want to install the package
* adjust version of the package in accordance to packages you download
* adjust certificate information for the HTTPS SOLIDserver communication

# run playbook
```
ansible-playbook playbooks/deploy.yml
```
